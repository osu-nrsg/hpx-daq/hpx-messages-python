# HPx Messages

This package provides dataclasses for unpacking msgpack-packed STATUS, CONFIG, and RAY messages from the
[HPx Radar Server](https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server)

## Usage

```python
import zmq

from hpx_messages import ConfigMsg, RayMsg, ServerState, StatusMsg

endpoint = "ipc://WIMR.sock"
config_sub = zmq.Context.instance().socket(zmq.SUB)
status_sub = zmq.Context.instance().socket(zmq.SUB)
ray_sub = zmq.Context.instance().socket(zmq.SUB)

config_sub.subscribe(b"CONFIG")
status_sub.subscribe(b"STATUS")
ray_sub.subscribe(b"RAY")

# Get a status message
status_sub.connect(endpoint)
status = StatusMsg.fromsocket(ray_sub)
status_sub.disconnect()

# check server state enum
if status.state != ServerState.running:
    print(f"The server is not running. State: {status.state}")
    # will print the ServerState enum repr

# Get a config message with zeromq and create a ConfigMsg from the data
config_sub.connect(endpoint)
parts = config_sub.recv_multipart()
config = ConfigMsg.frombytes(parts[1])
config_sub.disconnect()

# Use the config to preallocate the ray message then get ten rays and print the calculated timestamp on each
# ray.
ray = RayMsg.preallocate(config.num_samples)
ray_sub.connect(endpoint)
for _ in range(10):
    ray.update_fromsocket(ray_sub)
    print(ray.calc_time)
ray_sub.disconnect()

```
