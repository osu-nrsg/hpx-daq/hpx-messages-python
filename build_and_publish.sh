#!/bin/bash

PROJECT_NAME=$(.venv/bin/python - << DOC
import toml
with open("pyproject.toml") as f:
    pp = toml.load(f)
    print(pp["tool"]["poetry"]["name"])
DOC
)

if [ $? -ne 0 ]; then
  echo "Could not get project name. Exiting." &1>2
  exit 1
fi

rm -rf dist build
poetry build

REMOTE_DIR=/nfs/depot/cce_u1/haller/shared/public_html/pypi/$PROJECT_NAME/

rsync -rtP dist/ access.engr.oregonstate.edu:$REMOTE_DIR
ssh access.engr.oregonstate.edu chmod -vR a+rX $REMOTE_DIR
