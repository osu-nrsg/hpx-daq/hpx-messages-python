"""
Classes to unpack and store the HPx Radar Server messages.
"""
from __future__ import annotations

import abc
import enum
import time
from dataclasses import dataclass
from datetime import datetime, timedelta
from pathlib import Path
from typing import Callable, Dict, Final, Optional, Type, TypeVar

import msgpack
import numpy as np
import single_version
import zmq

__all__ = ["ConfigMsg", "ConfigMsgError", "RayMsg", "ServerState", "StatusMsg", "TpgMode"]
__version__ = single_version.get_version(__name__, Path(__file__).parent)


c0: Final = 2.997e8

SMT = TypeVar("SMT", bound="ServerMsgBase")

ConverterDictT = Dict[str, Callable]


@dataclass
class ServerMsgBase(abc.ABC):
    """Base class for *Msg dataclasses. Includes a method to create from msgpack packed bytes."""

    @classmethod
    def frombytes(cls: Type[SMT], message: bytes, converters: Optional[ConverterDictT] = None) -> SMT:
        """Unpack the msgpack-packed bytes into the dataclass."""

        def convert_kwargs(kwargs):
            if converters:
                for k, v in kwargs.items():
                    if k in converters:
                        kwargs[k] = converters[k](v)
            return kwargs

        return msgpack.unpackb(message, object_hook=lambda kwargs: cls(**convert_kwargs(kwargs)))

    @classmethod
    def fromsocket(cls: Type[SMT], sock: zmq.Socket, block=False) -> Optional[SMT]:
        """Attempt to create the message by receiving from a 0MQ socket.

        Parameters
        ----------
        sock
            zmq.Socket to receive the message
        block
            If True, wait indefinitely to receive a message. Otherwise return `None` if no message is
            available.
        """
        flags: int = 0 if block else zmq.DONTWAIT
        try:
            parts = sock.recv_multipart(flags=flags)
            return cls.frombytes(parts[1])
        except zmq.Again:
            return None

    @classmethod
    def csv_header(cls: Type[SMT]):
        return ",".join(cls.__dataclass_fields__)


@enum.unique
class ServerState(enum.Enum):
    """Possible states of the HPx Radar Server."""

    begin = 0
    config_and_start = 1
    running = 2
    running_alarm = 3
    stopping = 4
    stopped = 5
    stopped_config_error = 6
    stopped_run_error = 7
    end = 8


@enum.unique
class TpgMode(enum.Enum):
    """Modes for the test pattern generator"""

    analog_ramp = 0
    digital_pulse = 1
    digital_ramp = 2
    offset_dac = 3


class ConfigMsgError(Exception):
    pass


@dataclass
class ConfigMsg(ServerMsgBase):
    """CONFIG message from the HPx Radar Server."""

    board_idx: int
    analog_channel: str
    num_samples: int
    start_range_metres: float
    range_correction_metres: float
    voltage_offset: float
    gain: float
    tpg_enabled: bool
    # tpg_mode: TpgMode  # see below
    tpg_trigger_frequency: float
    tpg_acp_frequency: float
    tpg_arp_frequency: int
    enable_azimuth_interpolation: bool
    status_and_config_period_ms: int
    spokes_per_interrupt: int
    interrupts_per_second: int
    calc_true_timestamps: bool
    summing_factor: int
    start_azival: int
    end_azival: int
    send_filtered: bool
    card_name: str
    fpga_version: int
    calc_start_range_metres: float
    calc_end_range_metres: float
    meas_spokes_per_interrupt: int
    version: str
    tpg_mode: TpgMode = TpgMode.analog_ramp  # should be in hpx-radar-server v0.6.0
    bit_depth: int = 12  # not in CONFIG message for now.

    @classmethod
    def frombytes(cls: Type[ConfigMsg], message: bytes) -> ConfigMsg:
        # overload frombytes method to specify converters
        converters = {
            "tpg_mode": TpgMode,
        }
        return super().frombytes(message, converters)

    def __eq__(self, o: ConfigMsg) -> bool:
        eq = True
        for k in self.__dataclass_fields__:
            if k.startswith("calc") or k.startswith("meas"):
                continue
            else:
                eq &= getattr(self, k) == getattr(o, k)
        return eq

    @property
    def range_bin_size(self):
        return c0 / (2 * self.sampling_frequency)

    @property
    def daq_hardware_type(self):
        return "Cambridge Pixel"

    @property
    def sampling_frequency(self) -> float:
        card_sampling_frequencies = {
            "200": 50e6,
            "250": 50e6,
            "400": 100e6,
            "410": 125e6,
        }
        for str_, f in card_sampling_frequencies.items():
            if str_ in self.card_name:
                return f
        else:
            raise ConfigMsgError("Cannot get sampling frequency based on card name.")

    @property
    def start_azid(self) -> float:
        """start_azival in degrees"""
        return 360.0 * self.start_azival / 2 ** 16

    @property
    def end_azid(self) -> float:
        """end_azival in degrees"""
        return 360.0 * self.end_azival / 2 ** 16


@dataclass
class StatusMsg(ServerMsgBase):
    """STATUS message from the HPx Radar Server"""

    state: ServerState
    avg_prf: float
    meas_azimuths: int
    meas_rays: int
    runtime_s: int
    message: str

    @classmethod
    def frombytes(cls: Type[ConfigMsg], message: bytes) -> ConfigMsg:
        # overload frombytes method to specify converters
        converters = {
            "state": ServerState,
        }
        return super().frombytes(message, converters)


@dataclass(init=False)
class RayMsg(ServerMsgBase):
    """RAY message from HPx Radar Server.

    Rather than create a instance of this, it is designed to be reused with the update method.
    """

    __slots__ = (
        "rotation_idx",
        "ray_idx",
        "memory_bank",
        "proc_time",
        "time_interval",
        "calc_time",
        "azimuth",
        "data",
        "filtered",
    )
    rotation_idx: int
    ray_idx: int
    memory_bank: int
    proc_time: datetime
    time_interval: timedelta
    calc_time: Optional[datetime]
    azimuth: np.uint16
    data: np.ndarray
    filtered: bool

    def __init__(
        self,
        rotation_idx: int,
        ray_idx: int,
        memory_bank: int,
        proc_time: msgpack.Timestamp,
        time_interval: int,
        calc_time: msgpack.Timestamp,
        azimuth: np.uint16,
        data: bytes,
    ):
        self.update(
            rotation_idx,
            ray_idx,
            memory_bank,
            proc_time,
            time_interval,
            calc_time,
            azimuth,
            data,
        )

    @classmethod
    def preallocate(cls, nsamples: int) -> RayMsg:
        return cls(
            0,
            0,
            0,
            msgpack.Timestamp(0),
            0,
            msgpack.Timestamp(0),
            np.uint16(0),
            np.zeros(nsamples, np.uint16).tobytes(),
        )

    def update_frombytes(self, message: bytes):
        """Like ServerMsgBase.frombytes, but updates existing message."""
        msgpack.unpackb(message, object_hook=lambda kwargs: self.update(**kwargs))

    def update_fromsocket(self, sock: zmq.Socket, block: bool = True) -> bool:
        """Like ServerMsgBase.fromsocket, but updates existing message.

        Parameters
        ----------
        sock
            0MQ SUB socket receiving the ray message.
        block : optional
            If True, block until message received or sock.recvtimeo (ms) exceeded. Otherwise return
            immediately if none is available. Default True.

        Returns
        -------
        bool
            True if succesfully updated the message, False if blocking and timed out or nonblocking and no
            message available.
        """
        flags = 0 if block else zmq.NOBLOCK
        try:
            parts = sock.recv_multipart(flags=flags)
            self.update_frombytes(parts[1])
            return True
        except zmq.Again:
            return False

    def update(
        self,
        rotation_idx: int,
        ray_idx: int,
        memory_bank: int,
        proc_time: msgpack.Timestamp,
        time_interval: int,
        calc_time: msgpack.Timestamp,
        azimuth: np.uint16,
        data: bytes,
    ):
        self.rotation_idx = rotation_idx
        self.ray_idx = ray_idx
        self.memory_bank = memory_bank
        self.proc_time = proc_time.to_datetime()
        self.time_interval = timedelta(microseconds=time_interval)
        self.calc_time = calc_time.to_datetime()
        self.azimuth = azimuth
        self.filtered = data == b"\xFF\xFF"
        try:
            self.data[...] = 0 if self.filtered else np.frombuffer(data, dtype=np.uint16)
        except (AttributeError, ValueError):
            # self.data not yet initialized or size mismatch
            if self.filtered:
                self.data = np.zeros(0, np.uint16)
            else:
                self.data = np.frombuffer(data, dtype=np.uint16)


if __name__ == "__main__":
    import msgpack
    import zmq

    sweeps = []
    context = zmq.Context()
    status_sub = context.socket(zmq.SUB)
    status_sub.subscribe("STATUS")
    status_sub.connect("ipc:///shared/sockets/WIMR.sock")
    config_sub = context.socket(zmq.SUB)
    config_sub.subscribe("CONFIG")
    config_sub.connect("ipc:///shared/sockets/WIMR.sock")
    ray_sub = context.socket(zmq.SUB)
    ray_sub.subscribe("RAY")
    ray_sub.connect("ipc:///shared/sockets/WIMR.sock")

    ray = None
    last_dt = None
    max_td = timedelta(0)
    min_td = timedelta(seconds=3600)
    while True:
        ray_parts = None
        status_parts = None
        config_parts = None
        try:
            config_parts = config_sub.recv_multipart(zmq.NOBLOCK)
        except zmq.Again:
            pass
        else:
            config = ConfigMsg.frombytes(config_parts[1])
            print(config)
            if not ray:
                ray = RayMsg.preallocate(config.num_samples)
        if not ray:
            continue
        try:
            status_parts = status_sub.recv_multipart(zmq.NOBLOCK)
        except zmq.Again:
            pass
        else:
            status = StatusMsg.frombytes(status_parts[1])
            print(status)

        # Get ray message, if possible
        try:
            ray_parts = ray_sub.recv_multipart(zmq.NOBLOCK)
        except zmq.Again:
            time.sleep(0.00001)
        else:
            ray.update_frombytes(ray_parts[1])
