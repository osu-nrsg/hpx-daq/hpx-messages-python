import struct
from pathlib import Path

import toml

import hpx_messages
from hpx_messages import ConfigMsg, ServerState, StatusMsg, TpgMode


def test_version():
    ppt_path = Path(__file__).parent.parent / "pyproject.toml"
    config = toml.load(ppt_path)
    assert config["tool"]["poetry"]["version"] == hpx_messages.__version__


def test_status_frombytes():
    payload = (
        b"\x86\xa5state\x02\xa7avg_prf\xcb@\xa2\xe3\x97\x06o\xe8;\xadmeas_azimuths\xcd\x01"
        b"h\xa9meas_rays\xcd\x0e>\xa9runtime_s\xce\x00\x01%7\xa7message\xa0"
    )
    assert StatusMsg.frombytes(payload) == StatusMsg(
        state=ServerState.running,
        avg_prf=struct.unpack("d", b"@\xa2\xe3\x97\x06o\xe8;"[::-1])[0],
        meas_azimuths=360,
        meas_rays=3646,
        runtime_s=75063,
        message="",
    )


def test_config_frombytes():
    payload = (
        b"\xde\x00\x1a"
        b"\xa9board_idx\x00"
        b"\xaeanalog_channelA"
        b"\xabnum_samples\xcd\x10\x00"
        b"\xb2start_range_metres\xcb@}\xe0\x00\x00\x00\x00\x00"
        b"\xb7range_correction_metres\xcb\xc0\x95\xe0\x00\x00\x00\x00\x00"
        b"\xaevoltage_offset\xcb@\x06\x00\x00\x00\x00\x00\x00"
        b"\xa4gain\xcb"
        b"\xbf\xf0\x00\x00\x00\x00\x00\x00"
        b"\xabtpg_enabled\xc2"
        b"\xb5tpg_trigger_frequency\xcb@\x9f@\x00\x00\x00\x00\x00"
        b"\xb1tpg_acp_frequency\xcb@"
        b"\xa4\x00\x00\x00\x00\x00\x00"
        b"\xb1tpg_arp_frequency\xcd\x08\x00"
        b"\xbcenable_azimuth_interpolation\xc3"
        b"\xbbstatus_and_config_period_ms\xcd\x01\xf4"
        b"\xb4spokes_per_interrupt\xff"
        b"\xb5interrupts_per_second\xff"
        b"\xb4calc_true_timestamps\xc3"
        b"\xaesumming_factor\x06"
        b"\xacstart_azival\x00"
        b"\xaaend_azival\x00"
        b"\xadsend_filtered\xc3"
        b"\xa9card_name\xa7HPx-200"
        b"\xacfpga_version\x11"
        b"\xb7calc_start_range_metres\xcb@}\xcb6l\x83`\xf8"
        b"\xb5calc_end_range_metres\xcb@\xc8\xea\x19\x95;\xe6\xe2"
        b"\xb9meas_spokes_per_interruptP"
        b"\xa7version\xa50.5.0"
    )
    assert ConfigMsg.frombytes(payload) == ConfigMsg(
        board_idx=0,
        analog_channel=65,
        num_samples=4096,
        start_range_metres=478.0,
        range_correction_metres=-1400.0,
        voltage_offset=2.75,
        gain=-1.0,
        tpg_enabled=False,
        tpg_trigger_frequency=2000.0,
        tpg_acp_frequency=2560.0,
        tpg_arp_frequency=2048,
        enable_azimuth_interpolation=True,
        status_and_config_period_ms=500,
        spokes_per_interrupt=-1,
        interrupts_per_second=-1,
        calc_true_timestamps=True,
        summing_factor=6,
        start_azival=0,
        end_azival=0,
        send_filtered=True,
        card_name="HPx-200",
        fpga_version=17,
        calc_start_range_metres=476.7007870800003,
        calc_end_range_metres=12756.199866760002,
        meas_spokes_per_interrupt=80,
        version="0.5.0",
        tpg_mode=TpgMode.analog_ramp,
        bit_depth=12,
    )
