# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2021-05-21

- Add sampling frequencies of other HPx cards
- Convert package to single file

## [1.0.0] - 2021-03-04

- Handle tpg_mode in ConfigMsg
- Add a few tests

## [0.2.0] - 2021-02-10

- Removed `gain_dB` property from `ConfigMsg`.

## [0.1.4] - 2020-11-23

- Using the `single-version` package instead of custom `get_version()`

## [0.1.3] -  2020-11-19

### Modified

- `ConfigMsg.__eq__` method ignores `calc_` and `meas_` fields.

## [0.1.2] -  2020-10-27

### Added

- `_version.py` with `get_version`
- `upload_to_haller_pypi.sh` upload script

## [0.1.1] -  2020-10-02

### Added

- CHANGELOG.md
- README.md
- Get version from package version indirectly so `pyproject.toml` is the only home of the version

[Unreleased]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python/compare/v1.1.0...master
[1.1.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python/compare/v0.2.0...v1.0.0
[0.2.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python/-/tags/v0.1.1
